const isNull = (target: unknown): target is null => target === null
export default isNull
