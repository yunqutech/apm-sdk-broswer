import realType from './realType'

const isObject = (target: unknown): target is object =>
	realType(target) === 'Object'

export default isObject
