const hasProp = <T extends PropertyKey>(
	prop: T,
	val: object
): val is Record<T, unknown> => prop in val

export default hasProp
