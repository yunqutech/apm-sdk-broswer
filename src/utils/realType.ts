import isNull from './isNull'
import isUndefined from './isUndefined'

const realType = (target: unknown): string => {
	if (isNull(target)) {
		return 'null'
	}

	if (isUndefined(target)) {
		return 'undefind'
	}

	if (Number.isNaN(target)) {
		return 'NaN'
	}

	return (Object.prototype.toString.call(target) as string).slice(8, -1)
}

export default realType
