type Nil = null | unknown

const isNil = (target: unknown): target is Nil =>
	target === null || target === undefined
export default isNil
