import hasProp from './hasProp'
import isString from './isString'

const isErrorEvent = (val: object): val is ErrorEvent =>
	hasProp('type', val) &&
	isString(val.type) &&
	['ErrorBoundary', 'error'].includes(val.type)

export default isErrorEvent
