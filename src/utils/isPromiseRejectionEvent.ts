import hasProp from './hasProp'
import isString from './isString'

const isPromiseRejectionEvent = (val: object): val is PromiseRejectionEvent =>
	hasProp('type', val) &&
	isString(val.type) &&
	val.type === 'unhandledrejection'

export default isPromiseRejectionEvent
