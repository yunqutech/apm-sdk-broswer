const isUndefined = (target: unknown): target is undefined =>
	target === undefined
export default isUndefined
