import realType from './realType'

const isError = (val: unknown): val is Error => realType(val) === 'Error'
export default isError
