import send, { errorRecord2Blob } from './modules/send'
import parseError from './modules/ErrorMonitoring'

const sdk = ({ url }: { url: string }) => {
	function ErrorMonitoringHandle(err: ErrorEvent) {
		send(url, errorRecord2Blob(parseError(err)))
	}

	function ErrorMonitoringHandleAsync(err: PromiseRejectionEvent) {
		send(url, errorRecord2Blob(parseError(err)))
	}

	return {
		ErrorMonitoring() {
			window.addEventListener('error', ErrorMonitoringHandle)
			window.addEventListener('unhandledrejection', ErrorMonitoringHandleAsync)
		},
		removeErrorMonitoring() {
			window.removeEventListener('error', ErrorMonitoringHandle)
			window.removeEventListener(
				'unhandledrejection',
				ErrorMonitoringHandleAsync
			)
		},
		send(error: ErrorEvent | PromiseRejectionEvent) {
			send(url, errorRecord2Blob(parseError(error)))
		}
	}
}

export default sdk
