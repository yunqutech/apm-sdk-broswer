import send from '../send'

type TUrlRecord = {
	hostname: string
	port: string
	pathname: string
	protocol: string
}

const sdk = ({ hostname, port, pathname, protocol }: Partial<TUrlRecord>) => {
	const urlRecord: TUrlRecord = {
		hostname: hostname ?? window.location.hostname,
		port: port ?? window.location.port,
		pathname: pathname ?? window.location.pathname,
		protocol: protocol ?? window.location.protocol
	}

	return {
		map<U>(callback: (val: TUrlRecord) => U) {
			return callback(urlRecord)
		},

		ErrorMonitoring() {
			window.addEventListener('error', (err) => {
				send(productUrl(urlRecord), new Blob([err.error]))
			})

			window.addEventListener('unhandledrejection', (rejection) => {
				send(productUrl(urlRecord), new Blob([rejection.reason]))
			})
		}
	}
}

function productUrl({
	protocol,
	hostname,
	port,
	pathname
}: TUrlRecord): string {
	return `${protocol}//${hostname}:${port}${pathname}`
}

export { sdk as default }
