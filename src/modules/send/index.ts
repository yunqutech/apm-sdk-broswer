type ErrorRecord = {
	apm_error_message: string
	apm_error_type: string
	apm_error_detail: {
		function_name: string
		col_no: number
		line_no: number
		source_map_name: string
	}[]
	apm_error_analyze: boolean
}

/**
 * @description APM上报接口
 */
const send = (url: string, data: Blob) => window.navigator.sendBeacon(url, data)

/**
 * @description 错误记录转换为Blob数据结构
 * @param errorRecord 错误记录
 * @returns Blob1
 */
const errorRecord2Blob = (errorRecord: ErrorRecord) =>
	new Blob([JSON.stringify(errorRecord)], { type: 'application/json' })

export { send as default, errorRecord2Blob }
export type { ErrorRecord }
