import type { ErrorRecord } from '../send'
import isError from '../../utils/isError'
import isErrorEvent from '../../utils/isErrorEvent'
import isPromiseRejectionEvent from '../../utils/isPromiseRejectionEvent'

const parseError = (e: ErrorEvent | PromiseRejectionEvent): ErrorRecord => {
	const stack = getStackFromEvent(e)

	const parse: Pick<
		ErrorRecord,
		'apm_error_detail' | 'apm_error_message' | 'apm_error_type'
	> = {
		apm_error_type: isError(e) ? 'ErrorBoundary' : e?.type ?? '',
		apm_error_detail: getErrorDetail(stack),
		apm_error_message: getErrorMessage(stack)
	}

	return {
		apm_error_analyze: true,
		...parse
	}
}

function getStackFromEvent(e: ErrorEvent | PromiseRejectionEvent): string {
	if (isErrorEvent(e) && isError(e.error)) {
		return e.error.stack ?? ''
	}
	if (isPromiseRejectionEvent(e) && isError(e.reason)) {
		return e.reason.stack ?? ''
	}

	return ''
}

function getErrorDetail(stack: string): ErrorRecord['apm_error_detail'] {
	const parseStackResult = stack
		?.split('\n')
		?.filter((_, i) => i !== 0)
		?.map<ErrorRecord['apm_error_detail'][0]>(parseStackRecord)
		?.filter(({ col_no, line_no }) => !(col_no === 0 && line_no === 0))

	const findReactStackIndex = parseStackResult?.findIndex(
		({ source_map_name }) => source_map_name.includes('react')
	)

	return parseStackResult?.splice(
		0,
		findReactStackIndex === -1 ? parseStackResult.length : findReactStackIndex
	)
}

function parseStackRecord(record: string): ErrorRecord['apm_error_detail'][0] {
	const regex = /\s*at\s?(\w*)\s?\(?(http.+):(\d+):(\d+)\)?/
	const matches = record?.match(regex)

	return {
		function_name: matches?.[1] ?? '',
		source_map_name: matches?.[2] ? addMapToJSFileName(matches[2]) : '',
		line_no: matches?.[3] ? parseInt(matches[3], 10) : 0,
		col_no: matches?.[4] ? parseInt(matches[4], 10) : 0
	}
}

function getErrorMessage(stack: string): ErrorRecord['apm_error_message'] {
	return stack?.split('\n')?.[0] ?? ''
}

function addMapToJSFileName(fileName: string) {
	return fileName ? fileName + '.map' : ''
}

export default parseError
