module.exports = {
	extends: ['cz', 'gitmoji'],
	rules: {
		'header-max-length': [2, 'always', 72],
		'header-case': [2, 'always', 'lower-case'],
		'scope-case': [2, 'always', 'lower-case'],
		'body-leading-blank': [1, 'always'],
		'footer-leading-blank': [2, 'always'],
		'subject-full-stop': [2, 'never', '.'],
		'type-case': [2, 'always', 'lower-case']
		// 'type-empty': [2, 'never'],
		// 'subject-empty': [2, 'always'],
	}
	// parserPreset: {
	// 	parserOpts: {
	// 		headerPattern: /^(:\w*:)(?:\s)(?:\((.*?)\))?\s((?:.*(?=\())|.*)(?:\(#(\d*)\))?/,
	// 		headerCorrespondence: ['type', 'scope', 'subject', 'ticket'],
	// 	},
	// },
}
