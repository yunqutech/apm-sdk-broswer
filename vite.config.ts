/// <reference types="vitest" />
import { defineConfig, normalizePath } from 'vite'
import path from 'path'
import viteEslint from 'vite-plugin-eslint'

export default defineConfig(() => {
	return {
		root: normalizePath(path.join(__dirname, './src', 'index.ts')),
		resolve: {},
		plugins: [viteEslint()],

		build: {
			target: 'es2015',
			sourcemap: true,
			lib: {
				name: 'apm-sdk-browser',
				entry: normalizePath(path.join(__dirname, './src', 'index.ts')),
				fileName: 'apm-sdk-browser'
			},
			rollupOptions: {
				output: [
					{
						dir: 'dist/esm',
						format: 'esm'
					},
					{
						dir: 'dist/umd',
						format: 'umd',
						name: 'apm-sdk-browser'
					}
				]
			}
		},

		test: {
			coverage: {
				provider: 'v8',
				reporter: ['text', 'html'],
				reportsDirectory: normalizePath(
					path.join(__dirname, '/vitest/coverage')
				)
			},
			reporters: ['default', 'html'],
			outputFile: normalizePath(
				path.join(__dirname, '/vitest/report/index.html')
			)
		}
	}
})
