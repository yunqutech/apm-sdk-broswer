# apm
前端应用质量监控


## 启动
`git clone` 后进入项目根目录:
请确保安装了 `pnpm`: `npm install -g pnpm`

```bash
pnpm i
pnpm dev
```

## 技术栈
- `pnpm`
- `vite`
- `vitest`
- `typescript`